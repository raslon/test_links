FROM python:3.8

WORKDIR /links

EXPOSE 8000

ADD requirements.txt /links/

RUN python -m pip install --upgrade pip

RUN pip install --user -r requirements.txt

ADD . /links/

ENV PYTHONPATH "${PYTHONPATH}:/links/src"

ENV SERVER_HOST=0.0.0.0

ENV REDIS_HOST=redis