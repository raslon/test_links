from fastapi import APIRouter
from .links import router as links_router

router = APIRouter()
router.include_router(links_router)
