from fastapi import (
    APIRouter,
    Query
)

from urllib.parse import urlparse
import time
import redis

from ..settingis import settings

router = APIRouter()

db = redis.StrictRedis(host=settings.redis_host, db=0)


def parse_links(links_dict):
    domains = []
    for link in links_dict:
        domain = urlparse(link).netloc or urlparse(link).path
        if domain and domain not in domains:
            domains.append(domain)
    return domains


@router.post('/visited_links')
def save_links(
        links_dict: dict
):
    result = dict(status='')
    if 'links' in links_dict:
        domains = parse_links(links_dict['links'])
        for domain in domains:
            try:
                current_time = round(time.time())
                db.zadd('domains', {'{}:{}'.format(domain, current_time): current_time})
            except redis.exceptions.ConnectionError:
                result['status'] = 'Неполадки с бд'
                return result
        result['status'] = 'ok'
    else:
        result['status'] = 'Список ссылок отсутствует'

    return result


@router.get('/visited_domains')
def get_links(
        start_date: int = Query(..., alias='from'),
        end_date: int = Query(..., alias='to')
):
    result = dict(domains=[], status='')
    if start_date > end_date:
        result['status'] = 'Некорректный интервал времени'
        return result

    try:
        daties = db.zrangebyscore('domains', start_date, end_date, withscores=True)
    except redis.exceptions.ConnectionError:
        return dict(status='Неполадки с бд')
    links = sorted(list(set([data[0].decode('utf8').split(':')[0] for data in daties])))
    result.update(domains=links, status='ok')
    return result
