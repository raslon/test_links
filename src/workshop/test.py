import json
import time

import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
import requests
from .app import app
from .api.links import db

client = TestClient(app, base_url='http://127.0.0.1:8000')


def test_add_links():
    db.delete('domains')

    input_data = {
        "links": [
            "https://ya.ru",
            "https://ya.ru?q=123",
            "funbox.ru"
        ]
    }
    response = client.post("/visited_links", data=json.dumps(input_data))
    data = {"status": "ok"}

    assert response.json() == data


def test_add_links_without_data():
    db.delete('domains')
    input_data = {}
    response = client.post("/visited_links", data=json.dumps(input_data))
    data = {"status": "Список ссылок отсутствует"}

    assert response.json() == data


def test_get_domains_with_correct_data():
    db.delete('domains')
    current_time = round(time.time())
    input_data = {
        "links": [
            "https://ya.ru",
            "https://ya.ru?q=123",
            "funbox.ru",
            "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
        ]
    }
    client.post("/visited_links", data=json.dumps(input_data))

    response = client.get(f"/visited_domains?from={current_time}&to={current_time+200}")
    data = {
        "domains": [
            "funbox.ru",
            "stackoverflow.com",
            "ya.ru"
        ],
        "status": "ok"
    }
    assert response.json() == data


def test_get_domains_when_db_empty():
    db.delete('domains')

    response = client.get("/visited_domains?from=1&to=10")
    data = {
        "domains": [],
        "status": "ok"
    }
    assert response.json() == data
